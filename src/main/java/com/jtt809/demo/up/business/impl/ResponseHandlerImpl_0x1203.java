package com.jtt809.demo.up.business.impl;

import com.jtt809.demo.up.business.IBusinessServer;
import com.jtt809.demo.up.config.Jtt809Config;
import com.jtt809.demo.up.constant.ConstantJtt809;
import com.jtt809.demo.up.pojo.command.request.RequestJtt809_0x9101;
import com.jtt809.demo.up.pojo.command.response.ResponseJtt809_0x1203;
import com.jtt809.demo.up.util.*;
import io.netty.channel.ChannelHandlerContext;
import lombok.extern.slf4j.Slf4j;

import java.net.InetSocketAddress;

/**
 * 车辆动态信息交换业务
 * 链路类型：主链路
 * 消息方向：下级平台往上级平台
 * 业务数据类型标识：UP_EXG_MSG
 * 描述：下级平台向上级平台发送车辆动态信息交换业务数据包
 * <p>
 * 具体描述：
 * 车辆定位信息自动补报
 * 子业务类型标识：UP_EXG_MSG_HISTORY_LOCATION
 * 描述：车辆定位信息自动补报
 */
@Slf4j
public class ResponseHandlerImpl_0x1203 implements IBusinessServer<ResponseJtt809_0x1203> {

    public void businessHandler(ChannelHandlerContext ctx, ResponseJtt809_0x1203 msg) {
        try {
            String downPlatformKey = null;
            ChannelHandlerContext downPlatformSocket = null;
            if (Jtt809Config.JTT809_SLAVE_CREATE) {//有创建从链路
                InetSocketAddress upRemoteAddress = (InetSocketAddress) ctx.channel().remoteAddress();
                downPlatformKey = ConstantJtt809.UP_DOWN_PLATFORM_LINK.get(upRemoteAddress.toString());
                downPlatformSocket = ConstantJtt809.DOWN_PLATFORM.get(downPlatformKey);
            }

            //车辆动态信息缓存
            log.info("==========> 补录 {} 条车辆动态信息已执行RabbitMQ缓存", msg.getGpsSize());

            if (Jtt809Config.JTT809_SLAVE_CREATE) {//有创建从链路
                // 计数
                Object cacheObject = RedisUtil.get(RedisUtil.TYPE_UP_EXG_MSG_HISTORY_LOCATION_COUNT, downPlatformKey);
                int LOCATION_COUNT = 1;
                if (cacheObject == null) {
                    // 初始化
                    RedisUtil.set(RedisUtil.TYPE_UP_EXG_MSG_HISTORY_LOCATION_COUNT, downPlatformKey, 1);
                    RedisUtil.set(RedisUtil.TYPE_UP_EXG_MSG_HISTORY_LOCATION_STARTTIME, downPlatformKey, (System.currentTimeMillis() / 1000));
                } else {
                    LOCATION_COUNT = Integer.valueOf(cacheObject.toString());
                }

                if (LOCATION_COUNT >= Jtt809Config.JTT809_RECEIVE_GPS_OVERFLOW_NOTICE_COUNT) {
                    // 获取开始时间
                    long startTime = Long.valueOf(RedisUtil.get(RedisUtil.TYPE_UP_EXG_MSG_HISTORY_LOCATION_STARTTIME, downPlatformKey).toString());
                    // 获取结束时间
                    long endTime = System.currentTimeMillis() / 1000;
                    // 封装数据包
                    RequestJtt809_0x9101 requestJtt8090x9101 = new RequestJtt809_0x9101();
                    requestJtt8090x9101.setDynamicInfoTotal(LOCATION_COUNT);
                    requestJtt8090x9101.setStartTime(startTime);
                    requestJtt8090x9101.setEndTime(endTime);
                    // 发送
                    if (downPlatformSocket != null) {
                        downPlatformSocket.writeAndFlush(requestJtt8090x9101);
                        // 重置
                        RedisUtil.set(RedisUtil.TYPE_UP_EXG_MSG_HISTORY_LOCATION_COUNT, downPlatformKey, 1);
                        RedisUtil.set(RedisUtil.TYPE_UP_EXG_MSG_HISTORY_LOCATION_STARTTIME, downPlatformKey, (System.currentTimeMillis() / 1000));
                    }

                } else {
                    // 计数
                    RedisUtil.set(RedisUtil.TYPE_UP_EXG_MSG_HISTORY_LOCATION_COUNT, downPlatformKey, ++LOCATION_COUNT);
                }
            }
        } catch (Exception e) {
            log.error("车辆动态信息交换业务异常", e);
        }
    }

}
