package com.jtt809.demo.up.business.impl;

import com.jtt809.demo.up.business.IBusinessServer;
import com.jtt809.demo.up.pojo.command.request.RequestJtt809_0x1006;
import com.jtt809.demo.up.pojo.command.response.ResponseJtt809_0x1005;
import io.netty.channel.ChannelHandlerContext;

/**
 * 主链路连接保持请求消息
 * 链路类型:主链路。
 * 消息方向:下级平台往上级平台。
 * 业务数据类型标识:UP_LINKTEST_REQ。
 * 描述:下级平台向上级平台发送主链路连接保持清求消息，以保持主链路的连接。
 * 主链路连接保持清求消息，数据体为空。
 */
public class ResponseHandlerImpl_0x1005 implements IBusinessServer<ResponseJtt809_0x1005> {

    public void businessHandler(ChannelHandlerContext ctx, ResponseJtt809_0x1005 msg) {
        ctx.writeAndFlush(new RequestJtt809_0x1006());
    }
}
