package com.jtt809.demo.up.business.impl;

import com.jtt809.demo.up.business.IBusinessServer;
import com.jtt809.demo.up.pojo.command.response.ResponseJtt809_0x1601;
import io.netty.channel.ChannelHandlerContext;
import lombok.extern.slf4j.Slf4j;

/**
 * 车辆动态信息交换业务
 * 链路类型：主链路
 * 消息方向：下级平台往上级平台
 * 业务数据类型标识：UP_EXG_MSG
 * 描述：下级平台向上级平台发送车辆动态信息交换业务数据包
 * <p>
 * 具体描述：
 * 补发车辆静态信息应答
 * 子业务类型标识：UP_BASE_MSG_VEHICLE_ADDED_ACK
 * 描述：补发车辆静态信息应答
 */
@Slf4j
public class ResponseHandlerImpl_0x1601 implements IBusinessServer<ResponseJtt809_0x1601> {


    public void businessHandler(ChannelHandlerContext ctx, ResponseJtt809_0x1601 msg) {
        log.info("补发车辆静态信息应答{}", msg);
    }

}
