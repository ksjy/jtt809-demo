package com.jtt809.demo.up.business.impl;

import cn.hutool.core.util.RandomUtil;
import cn.hutool.core.util.ReUtil;
import cn.hutool.core.util.StrUtil;
import com.jtt809.demo.up.business.IBusinessServer;
import com.jtt809.demo.up.config.Jtt809Config;
import com.jtt809.demo.up.constant.Constant;
import com.jtt809.demo.up.constant.ConstantJtt809;
import com.jtt809.demo.up.manager.SlaveLinkManagerJtt809;
import com.jtt809.demo.up.pojo.command.request.RequestJtt809_0x1002;
import com.jtt809.demo.up.pojo.command.request.RequestJtt809_0x1002_Result;
import com.jtt809.demo.up.pojo.command.response.ResponseJtt809_0x1001;
import io.netty.channel.Channel;
import io.netty.channel.ChannelHandlerContext;
import lombok.extern.slf4j.Slf4j;

import java.net.InetSocketAddress;

/**
 * 主链路登录请求消息
 * 链路类型:主链路。
 * 消息方向:下级平台往上级平台。
 * 业务数据类型标识: UP_CONNECT_REQ
 * 描述:下级平台向上级平台发送用户名和密码等登录信息。
 */
@Slf4j
public class ResponseHandlerImpl_0x1001 implements IBusinessServer<ResponseJtt809_0x1001> {

    public void businessHandler(ChannelHandlerContext ctx, ResponseJtt809_0x1001 msg) {
        try {
            RequestJtt809_0x1002 jtt8090X1002 = new RequestJtt809_0x1002();

            int verifyCode = Jtt809Config.JTT809_FACTORY_ACCESS_ID;
            if (verifyCode <= 0) {
                // 随机生成校验码
                verifyCode = RandomUtil.randomInt(999999);
            }
            jtt8090X1002.setVerifyCode(verifyCode);
            jtt8090X1002.setEncryptFlag(msg.getEncryptFlag());

            if (msg.getUserId() != Jtt809Config.JTT809_NETTY_SERVER_DOWN_USERID) {
                // 用户没有注册
                jtt8090X1002.setResult(RequestJtt809_0x1002_Result.USER_NOT_REGISTERED);
                ctx.writeAndFlush(jtt8090X1002);
            } else if (msg.getUserId() == Jtt809Config.JTT809_NETTY_SERVER_DOWN_USERID
                    && !StrUtil.equalsIgnoreCase(msg.getPassword(), Jtt809Config.JTT809_NETTY_SERVER_DOWN_PASSWORD)) {
                // 密码错误
                jtt8090X1002.setResult(RequestJtt809_0x1002_Result.WRONG_PASSWORD);
                ctx.writeAndFlush(jtt8090X1002);
            } else if (!ReUtil.isMatch(Constant.REGEX_IS_IP, msg.getDownLinkIp())) {
                // 判断是否为内网ip || NetUtil.isInnerIP(msg.getDownLinkIp())
                // ip地址不正确
                jtt8090X1002.setResult(RequestJtt809_0x1002_Result.IP_ADDRESS_IS_ERROR);

                ctx.writeAndFlush(jtt8090X1002);
            } else if (msg.getMsgGesscenterId() != Jtt809Config.JTT809_FACTORY_ACCESS_CODE) {
                // 接入码不正确
                jtt8090X1002.setResult(RequestJtt809_0x1002_Result.ACCESS_CODE_IS_ERROR);

                ctx.writeAndFlush(jtt8090X1002);
            } else {
                // 正确
                jtt8090X1002.setResult(RequestJtt809_0x1002_Result.SCUUESS);
                ctx.writeAndFlush(jtt8090X1002);

                log.info("================> 主链路登录成功");

                if (Jtt809Config.JTT809_SLAVE_CREATE) {//创建从链路
                    // 延迟0.5s后连接
                    Thread.sleep(500);

                    // 建立从链路连接
                    Channel channel = new SlaveLinkManagerJtt809(msg.getDownLinkIp(), msg.getDownLinkPort(), verifyCode).start();

                    // 将上下级平台关联
                    InetSocketAddress upRemoteAddress = (InetSocketAddress) ctx.channel().remoteAddress();
                    InetSocketAddress downRemoteAddress = (InetSocketAddress) channel.remoteAddress();

                    log.info("================> upRemoteAddress = {}", upRemoteAddress);
                    log.info("================> downRemoteAddress = {}", downRemoteAddress);
                    ConstantJtt809.UP_DOWN_PLATFORM_LINK.put(upRemoteAddress.toString(), downRemoteAddress.toString());

                    //下级平台是否需要加密数据
                    ConstantJtt809.UP_DOWN_PLATFORM_ENCRYPT_FLAG.put(downRemoteAddress.toString(), msg.getEncryptFlag());
                }
            }
        } catch (Exception e) {
            log.error("主链路登录异常", e);
        }
    }

}
