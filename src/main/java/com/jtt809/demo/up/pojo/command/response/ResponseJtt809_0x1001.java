package com.jtt809.demo.up.pojo.command.response;

import com.jtt809.demo.up.pojo.BasePackage;
import com.jtt809.demo.up.pojo.Response;
import io.netty.buffer.ByteBuf;
import lombok.Data;

/**
 * 主链路登录请求消息
 * 链路类型:主链路。
 * 消息方向:下级平台往上级平台。
 * 业务数据类型标识: UP-CONNECT-REQ。
 * 描述:下级平台向上级平台发送用户名和密码等登录信息。
 */
@Data
public class ResponseJtt809_0x1001 extends Response {

    /**
     * 用户名
     */
    private long userId;

    /**
     * 密码
     */
    private String password;

    /**
     * 下级平台接入码，上级平台给下级平台分配唯一标识码
     */
    protected long msgGesscenterId;

    /**
     * 下级平台提供对应的从链路服务端 IP 地址
     */
    private String downLinkIp;

    /**
     * 下级平台提供对应的从链路服务器端口号
     */
    private int downLinkPort;

    @Override
    protected void decodeImpl(ByteBuf buf) {
        this.userId = buf.readUnsignedInt();
        this.password = (buf.readBytes(8).toString(BasePackage.DEFAULT_CHARSET_GBK)).trim();

        if (isJtt809Version2019()) {
            this.msgGesscenterId = buf.readUnsignedInt();
        }

        this.downLinkIp = (buf.readBytes(32).toString(BasePackage.DEFAULT_CHARSET_GBK)).trim();
        this.downLinkPort = buf.readUnsignedShort();
    }

}
