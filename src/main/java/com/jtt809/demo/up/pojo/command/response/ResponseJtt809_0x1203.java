package com.jtt809.demo.up.pojo.command.response;

import com.jtt809.demo.up.pojo.BasePackage;
import com.jtt809.demo.up.pojo.Location;
import com.jtt809.demo.up.util.BCDUtil;
import com.jtt809.demo.up.util.HexBytesUtil;
import com.jtt809.demo.up.util.TimeUtil;
import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;

import java.util.*;

/**
 * 车辆动态信息交换业务
 * 链路类型：主链路
 * 消息方向：下级平台往上级平台
 * 业务数据类型标识：UP_EXG_MSG
 * 描述：下级平台向上级平台发送车辆动态信息交换业务数据包
 *
 * 具体描述：
 * 车辆定位信息自动补报
 * 子业务类型标识：UP_EXG_MSG_HISTORY_LOCATION
 * 描述：车辆定位信息自动补报
 */
@Data
@Slf4j
public class ResponseJtt809_0x1203 extends ResponseJtt809_0x1200_VehiclePackage {

    /**
     * 车辆定位信息个数
     * 1>=gpsSize<=5
     */
    private int gpsSize;

    /**
     * 车辆定位信息
     */
    private List<Location> locations;

    @Override
    protected void decodeDataImpl(ByteBuf buf) {
        this.gpsSize = buf.readByte();
        this.locations = new ArrayList<>(this.gpsSize);
        for (int i = 0; i < this.gpsSize; i++) {
            Location location = new Location();

            if (isJtt809Version2019()) {
                location.setEncrypt(buf.readByte());
                location.setLocationDataLength((int) buf.readUnsignedInt());

                location.setAlarm(buf.readUnsignedInt());
                location.setState(buf.readUnsignedInt());
                location.setLat((buf.readUnsignedInt() / 1000000d));
                location.setLon((buf.readUnsignedInt() / 1000000d));
                location.setAltitude(buf.readUnsignedShort());
                location.setVec1(buf.readUnsignedShort() * 10);
                location.setDirection(buf.readUnsignedShort());

                try {
                    String datetimeStr = BCDUtil.bcd2Str(Unpooled.copiedBuffer(buf.readBytes(7)).array());
                    Date date = TimeUtil.parseUnsignedDatetime(datetimeStr);
                    Calendar calendar = Calendar.getInstance();
                    calendar.setTimeZone(TimeZone.getTimeZone("Asia/Shanghai"));//东八区
                    calendar.setTime(date);
                    location.setYear(calendar.get(Calendar.YEAR));
                    location.setMonth(calendar.get(Calendar.MONTH) + 1);
                    location.setDay(calendar.get(Calendar.DATE));
                    location.setHour(calendar.get(Calendar.HOUR_OF_DAY));
                    location.setMinute(calendar.get(Calendar.MINUTE));
                    location.setSecond(calendar.get(Calendar.SECOND));
                } catch (Exception e) {
                    log.error("2019版定位时间解析异常",e);
                }

                location.setPlatformId1(buf.readBytes(11).toString(BasePackage.DEFAULT_CHARSET_GBK).trim());
                location.setAlarm1(buf.readUnsignedInt());
                location.setPlatformId2(buf.readBytes(11).toString(BasePackage.DEFAULT_CHARSET_GBK).trim());
                location.setAlarm2(buf.readUnsignedInt());
                location.setPlatformId3(buf.readBytes(11).toString(BasePackage.DEFAULT_CHARSET_GBK).trim());
                location.setAlarm3(buf.readUnsignedInt());
            } else {
                location.setEncrypt(buf.readByte());
                location.setDay(buf.readByte());
                location.setMonth(buf.readByte());
                byte[] yearBytes = new byte[2];
                buf.readBytes(yearBytes);
                int year = Integer.parseInt(HexBytesUtil.bytesToHex(yearBytes),16);
                location.setYear(year);
                location.setHour(buf.readByte());
                location.setMinute(buf.readByte());
                location.setSecond(buf.readByte());
                location.setLon((buf.readUnsignedInt() / 1000000d));
                location.setLat((buf.readUnsignedInt() / 1000000d));
                location.setVec1(buf.readUnsignedShort());
                location.setVec2(buf.readUnsignedShort());
                location.setVec3(buf.readUnsignedInt());
                location.setDirection(buf.readUnsignedShort());
                location.setAltitude(buf.readUnsignedShort());
                location.setState(buf.readUnsignedInt());
                location.setAlarm(buf.readUnsignedInt());
            }
            locations.add(location);
        }

    }
}
