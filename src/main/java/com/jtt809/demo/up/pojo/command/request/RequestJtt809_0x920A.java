package com.jtt809.demo.up.pojo.command.request;

import com.jtt809.demo.up.constant.ConstantJtt809;
import io.netty.buffer.ByteBuf;
import lombok.Getter;
import lombok.Setter;

/**
 * 上报车辆驾驶员身份识别信息请求
 * 链路类型:从链路。
 * 消息方向:上级平台向下级平台下发。
 * 业务数据类型标识:DOWN_EXG_MSG_REPORT_DRIVER_INFO
 * 描述:上级平台向下级平台下发上报车辆驾驶员身份识别信息的请求消息，其数据体规定见表40
 */
@Getter
@Setter
public class RequestJtt809_0x920A extends RequestJtt809_0x1200_VehiclePackage {

    /**
     * 上传标识， 0x00 最新收到、0x01 从终端获取
     * 1 byte
     */
    private int flag;

    public RequestJtt809_0x920A() {
        super(ConstantJtt809.DOWN_EXG_MSG_REPORT_DRIVER_INFO);
        if (isJtt809Version2019()) {
            this.dataLength = 1;
        } else {
            this.dataLength = 0;
        }
    }

    @Override
    protected void encodeDataImpl(ByteBuf buf) {
        if (isJtt809Version2019()) {
            this.setFlag(buf.readByte());
        }
    }
}
