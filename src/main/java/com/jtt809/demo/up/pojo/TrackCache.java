package com.jtt809.demo.up.pojo;

import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

@Data
public class TrackCache implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 定位时间
     */
    private Date gpsTime;

    /**
     * 经度
     */
    private BigDecimal longitude;

    /**
     * 纬度
     */
    private BigDecimal latitude;

    /**
     * 车辆id
     */
    private Long carId;

    /**
     * 在线状态：1、在线 2、离线
     */
    private Integer onlineStatus;
    public static final int ONLINE_STATUS_ONLINE = 1;

}
