package com.jtt809.demo.up.util;

import lombok.extern.slf4j.Slf4j;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

@Slf4j
public class TimeUtil {

    public static final String DATETIME_FORMAT = "yyyy-MM-dd HH:mm:ss";
    public static final String DATETIME_FORMAT_UNSIGNED = "yyyyMMddHHmmss";
    public static final String DATETIME_FORMAT_UNSIGNED_GPS_BCD = "yyMMddHHmmss";

    public static long getUTCTime() {
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeZone(TimeZone.getTimeZone("UTC"));
        return calendar.getTime().getTime();
    }

    public static Date parseDatetime(String datetimeStr) throws ParseException {
        return new SimpleDateFormat(DATETIME_FORMAT).parse(datetimeStr);
    }

    public static Date parseUnsignedDatetime(String datetimeStr) throws ParseException {
        return new SimpleDateFormat(DATETIME_FORMAT_UNSIGNED).parse(datetimeStr);
    }

    public static Date parseGpsBcdUnsignedDatetime(String datetimeStr) throws ParseException {
        return new SimpleDateFormat(DATETIME_FORMAT_UNSIGNED_GPS_BCD).parse(datetimeStr);
    }

    public static String formatUnsignedNowDatetime() {
        return new SimpleDateFormat(DATETIME_FORMAT_UNSIGNED).format(new Date());
    }

    public static String formatDatetime(Date date) {
        return new SimpleDateFormat(DATETIME_FORMAT).format(date);
    }

    public static String formatGpsBcdUnsignedNowDatetime(Date date) {
        return new SimpleDateFormat(DATETIME_FORMAT_UNSIGNED_GPS_BCD).format(date);
    }

}
