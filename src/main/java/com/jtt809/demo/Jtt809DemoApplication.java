package com.jtt809.demo;

import org.springframework.boot.WebApplicationType;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
public class Jtt809DemoApplication {

    public static void main(String[] args) {
        new SpringApplicationBuilder(Jtt809DemoApplication.class)
                .web(WebApplicationType.NONE) // .REACTIVE, .SERVLET
//                .bannerMode(Banner.Mode.OFF)
                .run(args);
    }

}
