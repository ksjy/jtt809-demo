package com.jtt809.demo.down.pojo.command.request;

import com.jtt809.demo.up.constant.ConstantJtt809;
import io.netty.buffer.ByteBuf;
import lombok.Getter;
import lombok.Setter;

/**
 * 主链路静态信息交换消息
 * 链路类型：主链路
 * 消息方向：下级平台往上级平台
 * 业务数据类型标识：UP_BASE_MSG
 * 描述：下级平台向上级平台发送车辆动态信息交换业务数据包
 *
 * 具体描述：
 * 补发车辆静态信息应答
 * 子业务类型标识：UP_BASE_MSG_VEHICLE_ADDED_ACK
 * 描述：补发车辆静态信息应答
 */
@Setter
@Getter
public class RequestClientJtt809_0x1601 extends RequestClientJtt809_0x1600_VehiclePackage {

    /**
     * 车牌号
     */
    public static final String KEY_VIN = "VIN";
    /**
     * 车辆颜色
     */
    public static final String KEY_VEHICLE_COLOR = "VEHICLE_COLOR";
    /**
     * 车辆类型
     */
    public static final String KEY_VEHICLE_TYPE = "VEHICLE_TYPE";
    /**
     * 运输行业编码
     */
    public static final String KEY_TRANS_TYPE = "TRANS_TYPE";
    /**
     * 车籍地
     */
    public static final String KEY_VEHICLE_NATIONALITY = "VEHICLE_NATIONALITY";
    /**
     * 业户ID
     */
    public static final String KEY_OWERS_ID = "OWERS_ID";
    /**
     * 运输企业名称
     */
    public static final String KEY_OWERS_NAME = "OWERS_NAME";
    /**
     * 业户联系电话
     */
    public static final String KEY_OWERS_TEL = "OWERS_TEL";

    /**
     * 车辆静态信息
     * 车辆静态信息格式使用字符串表示，标识与内容之间用半角“:=”分开，不同标识以半角“;”为分隔符，如数据项为空，在“:=”后不加任何数值。表示如下:
     * 标识:=内容;标识:=内容。
     * 车辆静态信息数据体内容可根据实际情况按照JT/T 415进行扩充。数据体各字段要求规定见表70。
     * 完性数据示例:
     * VIN:=浙A25307; VEHICLE COLOR:=I;VEHICLE_ TYPE:=40
     * TRANS_TYPE:=030;VE1-IICLE	NATIONALIT:=330108;
     * OWERS_ID:=382738;OWERS_
     * NAME:=杭州货运代PP公司;
     * OWERS ORIG ID:=IOOO;OWERS TEL:=135168144990
     */
    private String carInfo;

    public RequestClientJtt809_0x1601(String carInfo) {
        super(ConstantJtt809.UP_BASE_MSG_VEHICLE_ADDED_ACK);
        this.carInfo = carInfo;
        super.setDataLength(carInfo.getBytes().length);
    }

    @Override
    protected void encodeDataImpl(ByteBuf buf) {
        buf.writeBytes(carInfo.getBytes());
    }
}
