package com.jtt809.demo.down.handler;

import cn.hutool.core.thread.ThreadUtil;
import cn.hutool.json.JSONUtil;
import com.jtt809.demo.down.PrimaryLinkClientManager;
import com.jtt809.demo.down.business.ClientBusinessFactory;
import com.jtt809.demo.down.pojo.command.request.RequestClientJtt809_0x1005;
import com.jtt809.demo.down.pojo.command.response.ResponseClientJtt809_0x1002;
import com.jtt809.demo.up.pojo.Response;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.EventLoop;
import io.netty.channel.SimpleChannelInboundHandler;
import io.netty.handler.timeout.IdleState;
import io.netty.handler.timeout.IdleStateEvent;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;

import java.util.concurrent.TimeUnit;

/**
 * 主链路处理器
 */
@Data
@Slf4j
public class PrimaryLinkClientJtt809Handler extends SimpleChannelInboundHandler<Response> {

    private String ip;

    private int port;

    private String downLinkIp;

    private int downLinkPort;

    private PrimaryLinkClientManager primaryLinkClient;

    public PrimaryLinkClientJtt809Handler(String ip, int port, String downLinkIp, int downLinkPort) {
        this.ip = ip;
        this.port = port;
        this.downLinkIp = downLinkIp;
        this.downLinkPort = downLinkPort;
        this.primaryLinkClient = new PrimaryLinkClientManager(ip, port, downLinkIp, downLinkPort);
    }

    @Override
    protected void channelRead0(ChannelHandlerContext ctx, Response msg) throws Exception {
        // 收到消息直接打印输出
        log.info("=====> 【下级平台|接收】指令 = 0x{} ， 数据 = {}", Integer.toHexString(msg.getMsgId()), JSONUtil.toJsonStr(msg));

        // 开启线程执行业务方法
        ThreadUtil.execute(new ClientBusinessFactory(ctx, msg));
    }

    @Override
    public void channelActive(ChannelHandlerContext ctx) throws Exception {
        super.channelActive(ctx);
    }

    @Override
    public void channelInactive(ChannelHandlerContext ctx) throws Exception {
        log.info("======> 【下级平台|信息】与上级平台服务端失去连接！");

        // 使用过程中断线重连
        final EventLoop eventLoop = ctx.channel().eventLoop();
        eventLoop.schedule(new Runnable() {
            public void run() {
                primaryLinkClient.start();
            }
        }, 1, TimeUnit.SECONDS);
        ctx.fireChannelInactive();
    }

    // 主链路心跳
    @Override
    public void userEventTriggered(ChannelHandlerContext ctx, Object evt) throws Exception {
        if (evt instanceof IdleStateEvent) {
            IdleStateEvent event = (IdleStateEvent) evt;
            if (event.state().equals(IdleState.READER_IDLE)) {
                log.info("======> 【下级平台|信息】长时间没收到上级平台推送的数据");
                // 超时关闭channel
                // ctx.close();
            } else if (event.state().equals(IdleState.WRITER_IDLE)) {
                // 上级平台登录成功，则发送心跳,保持长连接
                if (ResponseClientJtt809_0x1002.isIsLoginFlagFromUpPlatform()) {
                    ctx.channel().writeAndFlush(new RequestClientJtt809_0x1005());
                    log.info("======> 【下级平台|信息】心跳发送成功!");
                } else {
                    log.info("======> 【下级平台|信息】上级平台登录失败，不发送心跳!");
                }
            } else if (event.state().equals(IdleState.ALL_IDLE)) {
                log.info("======> 【下级平台|信息】ALL_IDLE");
            }
        }
        super.userEventTriggered(ctx, evt);
    }
}