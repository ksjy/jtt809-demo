package com.jtt809.demo.down.codec;

import cn.hutool.json.JSONUtil;
import com.jtt809.demo.up.pojo.BasePackage;
import io.netty.buffer.ByteBuf;
import io.netty.buffer.ByteBufUtil;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.MessageToByteEncoder;
import io.netty.util.ReferenceCountUtil;
import lombok.extern.slf4j.Slf4j;

/**
 * jtt809编码类
 */
@Slf4j
public class ClinetEncoderJtt809 extends MessageToByteEncoder<BasePackage> {

    protected void encode(ChannelHandlerContext ctx, BasePackage basePackage, ByteBuf out) {
        try {
            log.info("=====> 编码前的数据 {} ", JSONUtil.toJsonStr(basePackage));
            ByteBuf sendToMsg = basePackage.encode();

            log.info("=====> 【下级平台|发送】指令 = 0x{} ， 数据 = {}",Integer.toHexString(basePackage.getMsgId()), ByteBufUtil.hexDump(sendToMsg));
            out.writeBytes(sendToMsg);
            ReferenceCountUtil.release(sendToMsg);
        } catch (Exception e) {
            log.error("===========> 下级平台编码异常", e);
        }
    }
}
